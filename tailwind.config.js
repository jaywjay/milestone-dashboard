module.exports = {
  theme: {
    extend: {
      strokeWidth: {
        "3": "3",
        "4": "4",
      },
    },
  },
  variants: {},
  plugins: [],
};
