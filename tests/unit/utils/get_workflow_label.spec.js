import getWorkflowLabel from "@/utils/get_workflow_label";
import { WORKFLOW_VERIFICATION } from "@/constants";
import fillerLabels from "../../utils/filler_labels";

const WORKFLOW_LABEL = { title: WORKFLOW_VERIFICATION };

describe("getWorkflowLabel", () => {
  it("should find the workflow label when it's on its own", () => {
    const labels = [WORKFLOW_LABEL];

    expect(getWorkflowLabel(labels)).toEqual(WORKFLOW_LABEL.title);
  });

  it("should find the workflow label when other labels are present", () => {
    const labels = [...fillerLabels, WORKFLOW_LABEL];

    expect(getWorkflowLabel(labels)).toEqual(WORKFLOW_LABEL.title);
  });

  it("should find nothing when no workflow labels are present", () => {
    const labels = fillerLabels;

    expect(getWorkflowLabel(labels)).toEqual("");
  });

  it("should find nothing when no labels are present", () => {
    expect(getWorkflowLabel()).toEqual("");
  });
});
