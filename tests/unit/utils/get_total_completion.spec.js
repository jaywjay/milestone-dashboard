import getTotalCompletion from "@/utils/get_total_completion";
import generateIssues from "../../utils/generate_issues";

describe("getTotalCompletion", () => {
  it("should return 100% when all issues are completed", () => {
    const issues = generateIssues(2, { completion: 100 });

    expect(getTotalCompletion(issues)).toEqual(100);
  });

  it("should generate 0% when no issues are completed", () => {
    const issues = generateIssues(2, { completion: 0 });

    expect(getTotalCompletion(issues)).toEqual(0);
  });

  it("should return 68.75% with the following issues", () => {
    const issues = [
      ...generateIssues(2, { completion: 100 }),
      ...generateIssues(1, { completion: 0 }),
      ...generateIssues(1, { completion: 75 }),
    ];

    expect(getTotalCompletion(issues)).toEqual(68.75);
  });
});
