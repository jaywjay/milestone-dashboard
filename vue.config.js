const path = require("path");

module.exports = {
  publicPath: "/milestone-dashboard",
  configureWebpack: {
    resolve: {
      alias: {
        $: path.resolve(__dirname, "tests"),
      },
    },
  },
};
